# kytta/bookmarks

> Cool links I find online

Similar to the [awesome](https://awesome.re) or [delightful](https://delightful.club/)
lists, this repo hosts a giant list of links that I found interesting or
useful.

This list is dedicated to public domain (via [CC0](https://creativecommons.org/publicdomain/zero/1.0/)).

- [Articles, Documentation, and Guides](#articles-documentation-and-guides)
  - [Development](#development)
    - [Best practices](#best-practices)
      - [CLI](#cli)
      - [Web](#web)
    - [JavaScript and TypeScript](#javascript-and-typescript)
  - [Security](#security)
    - [Linux security](#linux-security)
    - [OpenPGP and GnuPG](#openpgp-and-gnupg)
- [Learning to Code](#learning-to-code)
  - [Web development](#web-development)
- [Libraries](#libraries)
  - [Python](#python)
- [Web Tools](#web-tools)
  - [JavaScript, TypeScript, Node.js, and Deno](#javascript-typescript-node-js-and-deno)
    - [Package optimization](#package-optimization)

## Articles, Documentation, and Guides

### Development

#### Best practices

- [**GNU Coding Standards > Program Behavior**](https://www.gnu.org/prep/standards/html_node/Program-Behavior.html)
  by _GNU_
- [**The Twelve-Factor App**](https://12factor.net/)

##### CLI

- [**Command Line Interface Guidelines**](https://clig.dev/)
  by Aanand Prasad, Cark Tashian, Ben Firshman, Eva Parish, et al.
- [**12 Factor CLI Apps**](https://medium.com/@jdxcode/12-factor-cli-apps-dd3c227a0e46)
  by Jeff Dickey of _Heroku_

##### Web

- [**How to Favicon in 2021: Six files that fit most needs**](https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs)
  by Andrey Sitnik

#### JavaScript and TypeScript

- [**TypeScript Documentation > JSDoc Reference**](https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html)
by _Microsoft_

### Security

#### Linux security

- [**Hardening the Linux server** (archived PDF)](https://web.archive.org/web/20140129225927/http://www.ibm.com/developerworks/linux/tutorials/l-harden-server/l-harden-server-pdf.pdf) by Jeffrey Orloff of _IBM_
- [**Linux Hardening Guide**](https://madaidans-insecurities.github.io/guides/linux-hardening.html) by madaidan
- [**Linux workstation security checklist**](https://github.com/lfit/itpol/blob/master/linux-workstation-security.md) by _the Linux Foundation IT_
- [**HowTos/OS_Protection**](https://wiki.centos.org/HowTos/OS_Protection) by _CentOS_
- [**Security**](https://wiki.archlinux.org/title/Security) by _Arch Linux_

#### OpenPGP and GnuPG

- [**How to digitally sign a file with multiple keys using GnuPG**](https://www.tylerburton.ca/2015/04/how-to-digitally-sign-a-file-with-multiple-keys-using-gnupg/)
  by Tyler Burton
- [**Key Transition**](https://riseup.net/en/security/message-security/openpgp/key-transition)
  by _riseup.net_
- **Series on GNU Privacy Guard** by Mike English of _Atomic Object_
  - [Getting Started with GNU Privacy Guard](https://spin.atomicobject.com/2013/09/25/gpg-gnu-privacy-guard/)
  - [Generating More Secure GPG Keys: Rationale](https://spin.atomicobject.com/2013/10/23/secure-gpg-keys/)
  - [Generating More Secure GPG Keys: A Step-by-Step Guide](https://spin.atomicobject.com/2013/11/24/secure-gpg-keys-guide/)
  - [Using an OpenPGP Smartcard with GnuPG](https://spin.atomicobject.com/2014/02/09/gnupg-openpgp-smartcard/)

## Learning to Code

### Web development

- [**Django Girls**](https://tutorial.djangogirls.org/) — easy-to-follow Django
  tutorial

## Libraries

### Python

- [**loguru — Python logging made (stupidly) simple**](https://github.com/Delgan/loguru)
  by Delgan

## Web Tools

### JavaScript, TypeScript, Node.js, and Deno

#### Package optimization

- [**Bundlephobia**](https://bundlephobia.com/) — find the cost of adding a npm 
  package to your bundle
- [**npm.anvaka.com**](https://npm.anvaka.com/#/) — Visualisation of npm
  dependencies
- [**Package Phobia**](https://packagephobia.com/) — find the cost of adding a
  new dev dependency to your project
